import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: '/step1', pathMatch: 'full' },
  { path: 'password', loadChildren: './password/password.module#PasswordPageModule' },
  { path: 'pin/:code/:mail', loadChildren: './pin/pin.module#PinPageModule' },
  { path: 'step1', loadChildren: './step1/step1.module#Step1PageModule' },
  { path: 'selection', loadChildren: './selection/selection.module#SelectionPageModule' },
  { path: 'loginSeller', loadChildren: './loginSeller/login-s.module#LoginSPageModule' },
  { path: 'loginUser', loadChildren: './loginUser/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'reg', loadChildren: './reg/reg.module#RegPageModule' },

  { path: 'user',
  canActivate: [AuthGuard] ,
   loadChildren: './user/user-routing.module#UserRoutingModule' },
  { path: 'seller',
  canActivate: [AuthGuard] ,
   loadChildren: './seller/seller-routing.module#SellerRoutingModule' },
  { path: 'repass/:mail', loadChildren: './repass/repass.module#RepassPageModule' },
  { path: 'marker', loadChildren: './marker/marker.module#MarkerPageModule' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
