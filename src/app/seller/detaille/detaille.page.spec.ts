import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaillesPage } from './detaille.page';

describe('DetaillePage', () => {
  let component: DetaillesPage;
  let fixture: ComponentFixture<DetaillesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetaillesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaillesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
