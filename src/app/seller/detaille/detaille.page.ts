import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { UserService } from '../../services/service.user';
import { SellerService } from '../../services/service.saller';
import { GoogleMaps, GoogleMapOptions, Marker, GoogleMap } from '@ionic-native/google-maps/ngx';

@Component({
  selector: 'app-detaille',
  templateUrl: './detaille.page.html',
  styleUrls: ['./detaille.page.scss'],
})
export class DetaillesPage implements OnInit {
  id: any ;
  mark: any;
  map: GoogleMap;
  lat: number;
  lng: number;
  pet: any ;
  user: any;
  listesCommenets: any;

  constructor( private route: ActivatedRoute, private nav: NavController,  private platform: Platform ,
    private service: UserService , private seller: SellerService , public googleMaps: GoogleMaps) { }

  async ngOnInit() {


    this.route.params.pipe().subscribe(params => {
      this.id = params ;
     });
     console.log(this.id.id) ;
      // tslint:disable-next-line:radix
     await this.service.getPubsById(parseInt(this.id.id)).toPromise().then( data => {
      console.log(data);
      this.mark = data[0] ;
    } ) ;
    await this.service.getCommentById( this.mark.id ).toPromise().then(data => {
      console.log(data) ;
      this.listesCommenets = data ;
    }) ;


      console.log(localStorage.getItem('token'));
     // tslint:disable-next-line:radix
     await this.seller.getSellerById(parseInt( localStorage.getItem('token'))).toPromise().then(
       data => {
        console.log(data);
        this.user = data ;
       } ) ;
       this.platform.ready();
       this.loadMap() ;


  }


  segmentChanged(ev: any) {
    console.log('Segment changed');
  }

  async loadMap() {
    // tslint:disable-next-line:radix
    const res = { 'lat': parseFloat( this.mark.x) , 'lng': parseFloat ( this.mark.y)  } ;
    await console.log(res) ;
    await console.log(res.lat);
    await console.log(res.lng);
    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat:  res.lat,
          lng:   res.lng
        },
        zoom: 15
            }
      ,
      controls: {
        'compass': false ,
        'indoorPicker': false
      } ,
    };
    this.map = this.googleMaps.create('map_canvas' , mapOptions );
  console.log('maps');
 // Add a marker
  const marker: Marker = this.map.addMarkerSync({
    'position': res,
    'title':  this.mark.name_pub
  });

  }
  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/seller/listes']) ;
  }

  async comnt() {
    const commenter = (<HTMLInputElement>document.getElementById('comnt')).value ;
    // tslint:disable-next-line:triple-equals
    if ( commenter != '' ) {
      console.log( commenter ) ;
        // tslint:disable-next-line:radix
        await this.service.postComment( this.mark.id  , commenter , parseInt( this.user.id ) ).toPromise()
        .then(data => {
          console.log(data) ;
        }) ;
    }
    console.log( 'hi ') ;
   await this.listesCommenet() ;

  }

  async listesCommenet() {
    await this.service.getCommentById( this.mark.id ).toPromise().then(data => {
      console.log(data) ;
      this.listesCommenets = data ;
    }) ;
  }


}
