import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NavParams, ToastController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { SellerService } from '../../services/service.saller';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.page.html',
  styleUrls: ['./add-project.page.scss'],
})


export class AddProjectPage implements OnInit {
  message: any;
  id: string;
  constructor(private route: ActivatedRoute , private ctrl: ToastController ,
     private service: SellerService , private nav: NavController) { }
  ngOnInit() {
    console.log('hi') ;
    this.route.params.pipe().subscribe(params => {
     this.message = params ;
    });
    console.log(this.message);
  }
  async click() {
    const nom = (<HTMLInputElement>document.getElementById('nom')).value ;
    const select = (<HTMLInputElement>document.getElementById('select')).value ;
    const desc = (<HTMLInputElement>document.getElementById('desc')).value ;
    this.id = localStorage.getItem('token') ;

    // tslint:disable-next-line:triple-equals
    if ( nom == '' || select == '' || desc == ''  ) {
      const toastt =  await this.ctrl.create({
        message: 'جميع الخانات اجبارية  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    } else {
      console.log(nom);
      console.log(this.id);
            // tslint:disable-next-line:radix
      console.log(parseInt(select));
      console.log(this.message.lat);
      console.log(this.message.lang);
      console.log(desc);
       // tslint:disable-next-line:radix
      await this.service.addProject ( nom , parseInt(this.id) , parseInt( select ) ,  this.message.lat , this.message.lang , desc )
      .toPromise().then(data => { console.log(data); } ) ;
      const toastt =  await this.ctrl.create({
        message: 'تم  التسجيل  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
      this.nav.navigateRoot('/seller/listes') ;
    }
  }

  function() {
    console.log('omar' );
  }

}
