import { Component, OnInit } from '@angular/core';
import { SellerService } from '../../services/service.saller';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {
  id: number;
  messages: any;
  msg: any;

  constructor( private service: SellerService , private cnt: NavController , private loadingCtrl: LoadingController) { }

 async  ngOnInit() {
  const loading = await  this.loadingCtrl.create({
    message: 'يرجى الانتظار ...'
  });
  loading.present();
    // tslint:disable-next-line:radix
    this.id = parseInt ( localStorage.getItem('token') ) ;
    console.log( this.id) ;
    await this.service.all_msg( this.id ).toPromise().then( data => {
      this.msg = data ;
      console.log(data) ;
    })
    .catch(() => {
      loading.dismiss();
      alert('تثبت من اتصالك بشبكة الأنترنت ') ;
    }) ;
    loading.dismiss();


  }

  goback() {
    console.log('hi' );
    this.cnt.navigateRoot(['/seller/listes']) ;
  }

  contenu(id) {
    this.cnt.navigateForward('/seller/contenu/' + id ) ;
  }

}
