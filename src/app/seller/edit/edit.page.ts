import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SellerService } from '../../services/service.saller';
import { GoogleMapOptions, GoogleMaps, GoogleMap, Marker } from '@ionic-native/google-maps/ngx';
import { Platform, ToastController, LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  id: any;
  pub: any;
  map: GoogleMap;

  constructor(private route: ActivatedRoute , private platform: Platform , private ctrl: ToastController , private nav: NavController
     , private service: SellerService , private googleMaps: GoogleMaps, private loadingCtrl: LoadingController) { }

  async ngOnInit() {
    await this.platform.ready();


    this.route.params.pipe().subscribe(params => {
      this.id = params ;
     });
     // tslint:disable-next-line:radix
     console.log(this.id.id) ;
     // tslint:disable-next-line:radix
     await this.service.pubById(parseInt(this.id.id)).toPromise().then(data => {
       this.pub = data[0] ;
       console.log(this.pub) ;
     });
     await this.loadMap();


  }

  async loadMap() {
    // tslint:disable-next-line:radix
    const res = { 'lat': parseFloat( this.pub.x) , 'lng': parseFloat ( this.pub.y)  } ;
    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat:  res.lat,
          lng:   res.lng
        },
        zoom: 15
            }
      ,
      controls: {
        'compass': false ,
        'indoorPicker': false
      } ,
    };
    this.map = this.googleMaps.create('map_canvas' , mapOptions );
  console.log('maps');
 // Add a marker
  const marker: Marker = this.map.addMarkerSync({
    'position': res,
    'title':  this.pub.name_pub
  });

  }

  async click() {
    const nom = (<HTMLInputElement>document.getElementById('nom')).value ;
    const select = (<HTMLInputElement>document.getElementById('select')).value ;
    const desc = (<HTMLInputElement>document.getElementById('desc')).value ;
     // tslint:disable-next-line:triple-equals
     if ( nom == '' || select == '' || desc == ''  ) {
      const toastt =  await this.ctrl.create({
        message: 'جميع الخانات اجبارية  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    } else {
      const loading = await  this.loadingCtrl.create({
        message: 'جاري التعديل  ...'
      });
      loading.present();
      // tslint:disable-next-line:radix
      await this.service.editPub(this.pub.id , nom , parseInt( select ), desc).toPromise().then(data => {
      console.log(data); }) ;
      loading.dismiss();
      alert('تم التعديل بنجاح ') ;
      this.nav.navigateRoot('seller/listes') ;
    }
  }

  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/seller/listes']) ;
  }

}
