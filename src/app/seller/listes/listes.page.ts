import { Component, OnInit } from '@angular/core';
import { SellerService } from '../../services/service.saller';
import { AppComponent } from '../../app.component';
import { ActionSheetController, LoadingController, AlertController, NavController } from '@ionic/angular';


@Component({
  selector: 'app-listes',
  templateUrl: './listes.page.html',
  styleUrls: ['./listes.page.scss'],
})

export class ListesPage implements OnInit {
    listes: any;
    open = true;
    tab: any[];
    id: string;

  constructor(private service: SellerService , private loadingCtrl: LoadingController , private alertController: AlertController
    , private app: AppComponent, public actionSheetController: ActionSheetController, private nav: NavController ) {
    this.id = localStorage.getItem('token') ;
    this.app.fun() ;
  }

 async  ngOnInit() {
   this.listes = null ;
   this.open = true ;
    // tslint:disable-next-line:radix
    await this.service.ListeByID( parseInt(this.id) ).toPromise().then( data => { this.listes = data ; console.log(data); } );
    // tslint:disable-next-line:triple-equals
    if (this.listes == '' ) {
      this.open = false ;
    } else {this.open = true ; }
    console.log( this.listes );
    this.tab = this.listes.data ;
  }

  async fun(id) {

    const actionSheet = await this.actionSheetController.create({
      header: 'خيارات',
      cssClass: 'action' ,
      buttons: [{
        text: '  حذف ',
        role: 'destructive',
        cssClass: 'primary' ,
        icon: 'trash',
        handler: () => {
          this.presentAlertConfirm(id) ;
          console.log('Delete clicked');
        }
      }, {
        text: '  تعديل ',
        icon: 'build',
        handler: () => {
          this.nav.navigateRoot('seller/edit/' + id ) ;
          console.log('Share clicked');
        }
      }, {
        text: '  إلغاء ',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

  }


  async deletePub(id: number) {
    const loading = await  this.loadingCtrl.create({
      message: 'يرجى الانتظار ...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
      alert('تثبت من اتصالك بشبكة الأنترنت ') ;
    }, 10000);

   await  this.service.deletePub(id).toPromise().then(data => {console.log(data) ; }) ;
   await this.ngOnInit() ;
   this.loadingCtrl.dismiss() ;
  }


  async presentAlertConfirm(id: number) {
    const alert = await this.alertController.create({
      header: 'متأكد !',
      message:  ' <strong> هل أنت متأكد من  </strong>الحذف !!!',
      buttons: [
        {
          text: 'حذف',
          cssClass: 'secondary',
          handler: () => {
            this.deletePub(id) ;
            console.log('Confirm Cancel');
          }
        },  {
          text: 'إلغاء',
          role: 'cancel',
          cssClass: 'primary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }

  detaille(id) {
    this.nav.navigateRoot('seller/detaille/' + id ) ;
  }


}
