import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { SellerService } from '../../services/service.saller';

@Component({
  selector: 'app-demande',
  templateUrl: './demande.page.html',
  styleUrls: ['./demande.page.scss'],
})
export class DemandePage implements OnInit {
  id: string;
  demande: any;
  user: number;

  constructor(private cnt: NavController , private service: SellerService , private loadingCtrl: LoadingController) { }

  async ngOnInit() {
    const loading = await  this.loadingCtrl.create({
      message: 'يرجى الانتظار ...'
    });
    loading.present();
    this.id = localStorage.getItem('token') ;
    // tslint:disable-next-line:radix
    this.user = parseInt(this.id) ;
    // tslint:disable-next-line:radix
    await this.service.get_demande().toPromise().then(data => {
      console.log(data) ;
      this.demande = data ;
    })
    .catch(() => {
      loading.dismiss();
      alert('تثبت من اتصالك بشبكة الأنترنت ') ;
    }) ;
    this.demande = this.demande.data ;
    console.log(this.demande) ;
    loading.dismiss();
  }


  goback() {
    console.log('hi' );
    this.cnt.navigateRoot(['/seller/listes']) ;
  }
}
