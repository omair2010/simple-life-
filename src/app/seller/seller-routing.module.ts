import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'addProject/:lat/:lang', loadChildren: './add-project/add-project.module#AddProjectPageModule' },
  { path: 'index', loadChildren: './index/index.module#IndexPageModule' },
  { path: 'listes', loadChildren: './listes/listes.module#ListesPageModule' },
  { path: 'messages', loadChildren: './messages/messages.module#MessagesPageModule' },
  { path: 'edit/:id', loadChildren: './edit/edit.module#EditPageModule' },
  { path: 'detaille/:id', loadChildren: './detaille/detaille.module#DetaillesPageModule' },
  { path: 'demande', loadChildren: './demande/demande.module#DemandePageModule' },
  { path: 'settings', loadChildren: './setting/settings.module#SettingsPageModule' },
  { path: 'contenu/:id', loadChildren: './contenu/contenu.module#ContenuPageModule' },








];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerRoutingModule { }
