import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { SellerService } from '../../services/service.saller';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contenu',
  templateUrl: './contenu.page.html',
  styleUrls: ['./contenu.page.scss'],
})
export class ContenuPage implements OnInit {
id: any ;
  msg: any;
  constructor(private nav: NavController , private service: SellerService , private route: ActivatedRoute) { }

  async ngOnInit() {
    this.route.params.pipe().subscribe(params => {
      this.id = params ;
     });
     console.log(this.id) ;

    // tslint:disable-next-line:radix
    await this.service.msgById( parseInt(this.id.id) ).toPromise().then( data => {
      this.msg = data ;
      console.log(this.msg) ;
    }) ;
  }

  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/seller/messages']) ;
  }

}
