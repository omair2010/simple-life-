import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenuPage } from './contenu.page';

describe('ContenuPage', () => {
  let component: ContenuPage;
  let fixture: ComponentFixture<ContenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
