import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Platform, NavController } from '@ionic/angular';
import { Component, OnInit, Output , EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  Marker,
  Geocoder,
  GeocoderResult,
} from '@ionic-native/google-maps/ngx';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {

  map: GoogleMap;
  search_address: any;
  isRunning: boolean;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  zone: any;
    latitude: number;
    longitude: number;
    autocompleteService: google.maps.places.AutocompleteService;
  saveDisabled: boolean;
  query = '';
  searchDisabled: boolean;
  places: any = [];
  latLng: any ;
  lat: number ;
  lng: any;
  constructor( private googleMaps: GoogleMaps , private app: AppComponent , private geolocation: Geolocation
     , private platform: Platform , private cnt: NavController) {
    this.saveDisabled = true ;
    this.saveDisabled = true ;


  }

    async ngOnInit() {
      this.app.fun();
      await this.platform.ready();
      await this.loadMap();
      this.searchDisabled = false ;

    }



  loadMap() {
    const mapOptions: GoogleMapOptions = {
      controls: {
        'compass': false ,
        'indoorPicker': false
      } ,
    };
    this.map = this.googleMaps.create('map_canvas' , mapOptions);
  console.log('maps');
    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(this.onMapClick.bind(this)) ;
    const address = (<HTMLInputElement>document.getElementById('search')) ;

  }

  searchPlace() {

    this.saveDisabled = true;
    if (this.query.length > 0 && ! this.searchDisabled) {

        const config = {
            types: ['geocode'],
            input: this.query
        } ;
        this.autocompleteService.getPlacePredictions(config, (predictions, status) => {

            // tslint:disable-next-line:triple-equals
            if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {

                this.places = [];

                predictions.forEach((prediction) => {
                    this.places.push(prediction);
                });
            }

        });

    } else {
        this.places = [];
    }

}

  onMapClick(params: any[]) {
    this.map.clear() ;
    this.latLng = params[0] ;
    console.log(this.latLng) ;
    const marker: Marker = this.map.addMarkerSync({
      icon: 'blue',
      animation: 'DROP',
      position: this.latLng
    });
     // Move to the position
     this.map.animateCamera({
      'target': marker.getPosition(),
      'zoom': 17
    }).then(() => {
      marker.showInfoWindow();
      this.isRunning = false;
    });
  }

  onButton1_click() {
    this.map.clear() ;

    const address = (<HTMLInputElement>document.getElementById('search')).value ;
    console.log(address) ;

    // Address -> latitude,longitude
    Geocoder.geocode({
      'address': address.toString()
        }).then((results: GeocoderResult[]) => {
      console.log(results);

      if (!results.length) {
        this.isRunning = false;
        return null;
      }

      // Add a marker
      const marker: Marker = this.map.addMarkerSync({
        'position': results[0].position,
      });
      this.latLng = results[0].position ;

      // Move to the position
      this.map.animateCamera({
        'target': marker.getPosition(),
        'zoom': 17
      }).then(() => {
        marker.showInfoWindow();
        this.isRunning = false;
      });
    });

  }

  click() {
    console.log(this.latLng) ;
    this.lat = this.latLng.lat ;
    this.lng = this.latLng.lng ;
    this.cnt.navigateRoot('seller/addProject/' + this.lat + '/' + this.lng ) ;
  }

  goback() {
    console.log('hi' );
    this.cnt.navigateRoot(['/seller/listes']) ;
  }

}
