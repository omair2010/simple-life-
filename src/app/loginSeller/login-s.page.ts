import { Component, OnInit } from '@angular/core';
import { SellerService } from '../services/service.saller';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login-s',
  templateUrl: './login-s.page.html',
  styleUrls: ['./login-s.page.scss'],
})
export class LoginSPage implements OnInit {
  users: any;
  i = 0;
  index: number;
  verif: any;
  user: any;

  constructor( private service: SellerService , private ctrl: ToastController , private router: Router ,
     private nav: NavController , private loadingCtrl: LoadingController) { }

  async ngOnInit() {
     // tslint:disable-next-line:triple-equals
     if (localStorage.getItem('isLoggedIn') == 'true' ) {
       console.log(localStorage.getItem('token'));
      // tslint:disable-next-line:radix
      await this.service.getSellerById(parseInt( localStorage.getItem('token'))).toPromise().then(data => {
        console.log(data);
        this.user = data ;
      } ) ;
     // tslint:disable-next-line:triple-equals
      if ( this.user.type == 's' ) {
        this.router.navigate(['/seller/home']) ;
      }
     }
    await this.service.getSeller().toPromise().then(data => {this.users = data ; } ) ;
    // tslint:disable-next-line:triple-equals
    console.log(this.users.data[0].password == '123456') ;
  }
  async login( mail , pass ) {
      const loading = await  this.loadingCtrl.create({
        message: 'يرجى الانتظار ...'
      });
      loading.present() ;
    this.verif = undefined ;
  await this.service.getSellerMail(mail).toPromise().then( data => { this.users = data ; } ) ;
 console.log(this.users) ;
 await this.service.loginPassword(mail , pass ).toPromise().then(data => { console.log(data) ; this.verif = data ; }) ;
 console.log(this.verif.id);
  // tslint:disable-next-line:triple-equals
  if (this.users.email == mail && this.users.type == 's' ) {
     // tslint:disable-next-line:triple-equals
     if ( this.verif.id != undefined  ) {
       console.log('login') ;
       localStorage.setItem('isLoggedIn', 'true');
       localStorage.setItem('token', this.users.id);
       this.router.navigate(['/seller/listes']) ;
       loading.dismiss();
     } else { console.log( 'password invalid' ) ;
     loading.dismiss();
     const toastt =  await this.ctrl.create({
      message: 'الرقم السري خاطئ  ',
      duration: 2000,
      position: 'bottom'
    });
    toastt.present();
   }
    } else { console.log( 'mail not found' );
    loading.dismiss();
    const toastt =  await this.ctrl.create({
      message: 'البريد الالكتروني خاطئ',
      duration: 2000,
      position: 'bottom'
    });
    toastt.present(); }
    setTimeout(() => {
      loading.dismiss();
    }, 6000);
  }

  async click() {
    const mail = (<HTMLInputElement>document.getElementById('mail')).value ;
    const pass =  (<HTMLInputElement>document.getElementById('pass')).value ;
    console.log(mail);
    console.log(pass) ;
    // tslint:disable-next-line:triple-equals
    if ( mail == '' && pass == ''  ) {
      const toastt =  await this.ctrl.create({
        message: 'لم تضع البريد ورقم السري ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    // tslint:disable-next-line:triple-equals
    } else if ( mail == ''  ) {
      const toastt =  await this.ctrl.create({
        message: 'لم تضع البريد  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    // tslint:disable-next-line:triple-equals
    } else if ( pass == '' ) {
      const toastt =  await this.ctrl.create({
        message: 'لم تضع رقم السري  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    } else { this.login(mail , pass ) ; }
  }

  href() {
    this.nav.navigateRoot(['/password']) ;
  }
  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/selection']) ;
  }
  go() {
    this.nav.navigateRoot(['/reg']) ;
  }
}
