import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginSPage } from './login-s.page';

describe('LoginSPage', () => {
  let component: LoginSPage;
  let fixture: ComponentFixture<LoginSPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginSPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginSPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
