import { Component, OnInit } from '@angular/core';
import { SellerService } from '../services/service.saller';
import { NavController } from '@ionic/angular';
import { UserService } from '../services/service.user';
@Component({
  selector: 'app-selection',
  templateUrl: './selection.page.html',
  styleUrls: ['./selection.page.scss'],
})
export class SelectionPage implements OnInit {
  id: any;

  constructor(  private service: SellerService , private router: NavController , private user: UserService) { }

  async ngOnInit() {


    // tslint:disable-next-line:triple-equals
    if (localStorage.getItem('isLoggedIn') == 'true' ) {
      console.log(localStorage.getItem('token'));
     // tslint:disable-next-line:radix
     await this.service.getSellerById(parseInt( localStorage.getItem('token'))).toPromise().then(
       data => { console.log(data);
        this.id = data ;
       } ) ;
            // tslint:disable-next-line:triple-equals
            if ( this.id.type == 's')  {this.router.navigateRoot(['/seller/home']) ;
          } else { this.router.navigateRoot(['/user/HomePage']) ; }
    }
  }

}
