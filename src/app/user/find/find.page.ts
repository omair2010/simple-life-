import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-find',
  templateUrl: './find.page.html',
  styleUrls: ['./find.page.scss'],
})
export class FindPage implements OnInit {
id: any ;
message: string ;
  constructor(private route: ActivatedRoute , private nav: NavController) {
   }

  ngOnInit() {
    this.route.params.pipe().subscribe(params => {
      this.id = params ;
     });
     console.log(this.id);
     // tslint:disable-next-line:triple-equals
     if (this.id.id == 1 ) {
        this.message = 'المشاغل النسائية' ;
     // tslint:disable-next-line:triple-equals
     } else if (this.id.id == 2 ) {
      this.message = 'محلات الجوال' ;
   // tslint:disable-next-line:triple-equals
   } else if (this.id.id == 3 ) {
    this.message = 'منتجات عزاب' ;
 // tslint:disable-next-line:triple-equals
 } else if (this.id.id == 4 ) {
  this.message = 'منتجات عائلية' ;
// tslint:disable-next-line:triple-equals
} else if (this.id.id == 5 ) {
  this.message = 'شاحنات طعام' ;
// tslint:disable-next-line:triple-equals
} else if (this.id.id == 6 ) {
  this.message = 'منتجات للمناسبات' ;
}
  }
  click() {
    const address = (<HTMLInputElement>document.getElementById('position')).value ;
       // tslint:disable-next-line:radix
     const id =  parseInt(this.id.id) ;
    this.nav.navigateRoot(['/user/map/' + address + '/' +  id ]) ;

  }
  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/user/HomePage']) ;
  }

}
