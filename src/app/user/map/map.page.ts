import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  GoogleMap, GoogleMapOptions, Environment, GoogleMaps, Marker, Geocoder, GeocoderResult, ILatLng, GoogleMapsEvent
} from '@ionic-native/google-maps/ngx';
import { Platform, NavController, LoadingController } from '@ionic/angular';
import { UserService } from '../../services/service.user';



@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  map: GoogleMap;
  isRunning: boolean;
  latLng: ILatLng;
  position: any ;
  mark: any;
  lat: number;
  lng: number;
  on = false;
  url: any;
  title = 'اضغط هنا بعد اختيار المكان';
  constructor( private platform: Platform , private service: UserService , private nav: NavController ,
    private googleMaps: GoogleMaps , private route: ActivatedRoute, private loadingCtrl: LoadingController ) { }

  async ngOnInit() {
    await this.platform.ready();
    this.route.params.pipe().subscribe(params => {
      this.position = params ;
     });
     console.log(this.position);
     // tslint:disable-next-line:radix
    await this.service.getPubs(parseInt(this.position.id) + 1 ).toPromise().then( data => {
        console.log(data);
        this.mark = data ;
      } ) ;
    await this.loadMap();
  }
  async loadMap() {
    await Geocoder.geocode({
      'address': this.position.position.toString()
        }).then((results: GeocoderResult[]) => {
      console.log(results[0].position);
      console.log(results[0].position.lng);
      this.lat = results[0].position.lat ;
      this.lng = results[0].position.lng ;
      console.log(this.lat);
      console.log(this.lng);
        } ) ;
    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat:  this.lat ,
          lng:   this.lng
        },
        zoom: 15
            }
      ,
      controls: {
        'compass': false ,
        'indoorPicker': false
      } ,
    };
    this.map = this.googleMaps.create('map_canvas' , mapOptions);
  console.log('maps');
  if ( this.mark.length > 1 ) {
  for ( const pos of  this.mark ) {
    await this.service.affiche(pos.id , localStorage.getItem('token')).subscribe((data: any) =>  {
      console.log(data.data.length);
      // tslint:disable-next-line:triple-equals
      if (data.data.length == 0 ) {
       this.onButton1_click( pos.x , pos.y , pos.name_pub , pos.id ) ;
      }
    });
  }
}

  }


   onButton1_click( lat , lng , title , id) {
    // Address -> latitude,longitude
      const res = { 'lat': lat , 'lng': lng  } ;

      // Add a marker
      const marker: Marker = this.map.addMarkerSync({
        'position': res,
        'title':  title
      });
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe( () =>  {
        this.title =  'تريد معرفة المزيد عن' + title ;
        this.on = true ;
        this.url = id ;
      }) ;

  }
  click() {
    if (this.on) {
        console.log(this.url) ;
        this.nav.navigateRoot(['user/plus/' + this.url]) ;
    }
  }

  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/user/find/' + this.position.id]) ;
  }

}
