
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  { path: 'find/:id', loadChildren: './find/find.module#FindPageModule' },
  { path: 'HomePage', loadChildren: './home-page/home-page.module#HomePagePageModule' },
  { path: 'setting', loadChildren: './setting/setting.module#SettingPageModule' },
  { path: 'map/:position/:id', loadChildren: './map/map.module#MapPageModule' },
  { path: 'plus/:id', loadChildren: './plus/plus.module#PlusPageModule' },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
