import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GoogleMapOptions, GoogleMap, GoogleMaps, Marker
} from '@ionic-native/google-maps/ngx';
import { Platform, NavController, AlertController, LoadingController } from '@ionic/angular';
import { UserService } from '../../services/service.user';
import { SellerService } from '../../services/service.saller';

@Component({
  selector: 'app-plus',
  templateUrl: './plus.page.html',
  styleUrls: ['./plus.page.scss'],
})
export class PlusPage implements OnInit {
id: any ;
  mark: any;
  map: GoogleMap;
  lat: number;
  lng: number;
  pet: any ;
  user: any;
  listesCommenets: any;
  tajer: any;

  constructor(
    private route: ActivatedRoute, private nav: NavController,  private platform: Platform , private alertController: AlertController
     , private service: UserService , private seller: SellerService , private googleMaps: GoogleMaps
     , private loadingCtrl: LoadingController) {

     }

  async ngOnInit() {
    await this.platform.ready();

    this.route.params.pipe().subscribe(params => {
      this.id = params ;
     });
      // tslint:disable-next-line:radix
     await this.service.getPubsById(parseInt(this.id.id)).toPromise().then( data => {
      console.log(data);
      this.mark = data[0] ;
    } ) ;

    await this.seller.getSellerById(this.mark.owner_pub).toPromise().then( data => {
      console.log(data);
      this.tajer = data ;
      console.log(this.tajer) ;
    } ) ;

    await this.service.getCommentById( this.mark.id ).toPromise().then(data => {
      console.log(data) ;
      this.listesCommenets = data ;
    }) ;


      console.log(localStorage.getItem('token'));
     // tslint:disable-next-line:radix
     await this.seller.getSellerById(parseInt( localStorage.getItem('token'))).toPromise().then(
       data => { console.log(data);
        this.user = data ;
       } ) ;
    await this.loadMap();
  }
  segmentChanged(ev: any) {
    console.log('Segment changed');
  }

  async loadMap() {
    // tslint:disable-next-line:radix
    const res = { 'lat': parseFloat( this.mark.x) , 'lng': parseFloat ( this.mark.y)  } ;
    await console.log(res) ;
    await console.log(res.lat);
    await console.log(res.lng);
    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat:  res.lat,
          lng:   res.lng
        },
        zoom: 15
            }
      ,
      controls: {
        'compass': false ,
        'indoorPicker': false
      } ,
    };
    this.map = this.googleMaps.create('map_canvas' , mapOptions );
  console.log('maps');
 // Add a marker
  const marker: Marker = this.map.addMarkerSync({
    'position': res,
    'title':  this.mark.name_pub
  });

  }
  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/user/HomePage']) ;
  }

  async comnt() {
    const commenter = (<HTMLInputElement>document.getElementById('comnt')).value ;
    // tslint:disable-next-line:triple-equals
    if ( commenter != '' ) {
      console.log( commenter ) ;
        // tslint:disable-next-line:radix
        await this.service.postComment( this.mark.id  , commenter , parseInt( this.user.id ) ).toPromise()
        .then(data => {
          console.log(data) ;
        }) ;
    }
    console.log( 'hi ') ;
   await this.listesCommenet() ;

  }

  async listesCommenet() {
    await this.service.getCommentById( this.mark.id ).toPromise().then(data => {
      console.log(data) ;
      this.listesCommenets = data ;
    }) ;
  }

  async messeage() {
    const alert = await this.alertController.create({
      header: '  إرسال  رسالة للإستفسار    ',
      inputs: [
        {
          name: 'subject',
          type: 'text',
          id: 'subject' ,
          placeholder: 'الموضوع  '
        },
        {
          name: 'name1',
          type: 'text',
          id: 'msg' ,
          placeholder: 'الرسالة '
        }
      ],
      buttons: [
        {
          text: 'إلغاء ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'إرسال',
          handler: () => {
            this.msg();
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
  }

  async demande() {
    console.log(this.mark.owner_pub );

      const alert = await this.alertController.create({
        header: 'طلب خدمة أو تحديد موعد ',
        inputs: [
          {
            name: 'name1',
            type: 'text',
            id: 'text' ,
            placeholder: 'طلب خدمة أو تحديد موعد'
          }
        ],
        buttons: [
          {
            text: 'إلغاء ',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'طلب',
            handler: () => {
              this.fn();
              console.log('Confirm Ok');
            }
          }
        ]
      });
      await alert.present();
  }

  async fn() {
    const loading = await  this.loadingCtrl.create({
      message: 'جاري الإرسال  ...'
    });
    loading.present();
    const text = (<HTMLInputElement>document.getElementById('text')).value ;
    console.log(text) ;
    // tslint:disable-next-line:radix
    await this.service.add_demande( parseInt( localStorage.getItem('token')) , this.mark.owner_pub , text , this.user.first_name  )
    .toPromise().then(
      data => {
        console.log(data) ;
        loading.dismiss() ;
        alert(' تم الإرسال ') ;
      }
    ).catch( () => {
      loading.dismiss() ;
      alert('تثبت من اتصالك بشبكة الأنترنت ') ;
    } ) ;

  }

  async msg() {

    const text = (<HTMLInputElement>document.getElementById('msg')).value ;
    const subject = (<HTMLInputElement>document.getElementById('subject')).value ;
    // tslint:disable-next-line:triple-equals
    if ( text == '' && subject == '' ) { alert('حدد الموضوع ومحتوى الرسالة ') ;
  } else {
    const loading = await  this.loadingCtrl.create({
      message: 'جاري الإرسال  ...'
    });
    loading.present();
    console.log(text) ;
    // tslint:disable-next-line:radix
    await this.seller.getSellerById(parseInt( localStorage.getItem('token'))).toPromise().then(
      data => { console.log(data);
       this.user = data ;
      } ) ;
    console.log(this.user) ;
    // tslint:disable-next-line:radix
    await this.seller.messages( subject , text , parseInt( localStorage.getItem('token'))  , this.mark.owner_pub , this.user.first_name  )
    .toPromise()
    .then(
      data => {
        console.log(data) ;
        loading.dismiss() ;
        alert(' تم الإرسال ') ;
      }
    )
    .catch( () => {
    loading.dismiss() ;
      alert('تثبت من اتصالك بشبكة الأنترنت ') ;
    }) ;

  }
  }


  async report() {
    const alert = await this.alertController.create({
      header: 'تبليغ عن تجاوزات  ',
      inputs: [
        {
          name: 'name1',
          type: 'text',
          id: 'text' ,
          placeholder: 'تبليغ '
        }
      ],
      buttons: [
        {
          text: 'إلغاء ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'إرسال',
          handler: () => {
            this.envoi();
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
  }
  envoi() {
    const text = (<HTMLInputElement>document.getElementById('text')).value ;
    // tslint:disable-next-line:radix
    this.service.report_pub(parseInt( this.tajer.id ) , text ,  parseInt(this.id.id)).subscribe( data => { console.log(data ) ; }) ;
  }



  async blocage() {
    const alert = await this.alertController.create({
      header: ' حجب المنشور    ',
      buttons: [
        {
          text: 'إلغاء ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'إرسال',
          handler: async () => {
            await this.service.blocage( this.id.id , localStorage.getItem('token') , 'blocage' )
            .toPromise().then(data => console.log(data)) ;
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
  }

}
