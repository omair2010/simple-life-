import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.page.html',
  styleUrls: ['./home-page.page.scss'],
})
export class HomePagePage implements OnInit {

  constructor(private app: AppComponent , private nav: NavController) {
    this.app.fun();
   }

  ngOnInit() {
  }

  go1() {
    this.nav.navigateRoot(['/user/find/' + 1 ]) ;
  }
  go2() {
    this.nav.navigateRoot(['/user/find/' + 2 ]) ;
  }
  go3() {
    this.nav.navigateRoot(['/user/find/' + 3 ]) ;
  }
  go4() {
    this.nav.navigateRoot(['/user/find/' + 4 ]) ;
  }
  go5() {
    this.nav.navigateRoot(['/user/find/' + 5 ]) ;
  }
  go6() {
    this.nav.navigateRoot(['/user/find/' + 6 ]) ;
  }

}
