import { Component, OnInit } from '@angular/core';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Camera , CameraOptions } from '@ionic-native/camera/ngx';
import { FTP } from '@ionic-native/ftp/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileTransferObject , FileUploadOptions } from '@ionic-native/file-transfer/ngx';
import { HttpHeaders } from '@angular/common/http';
import { UserService } from '../../services/service.user';
import { SellerService } from '../../services/service.saller';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {
 option: ImagePickerOptions ;
 myphoto: any ;
  id: string;
  open: any;
  url = '' ;
  name: any;
  prenom: any;
  password: any;
  constructor(private app: AppComponent, private transfert: FileTransfer , private file: File ,
    private service: UserService , private user: SellerService
      , private nav: NavController , private camera: Camera , private loadingCtrl: LoadingController , private cntr: ToastController) { }

  ngOnInit() {
  }
  async click() {
    const loading = await  this.loadingCtrl.create({
      message: 'Please wait...'
    });
    setTimeout(() => {
      loading.dismiss();
    }, 10000);

    loading.present();
    const fileTransfer: FileTransferObject = this.transfert.create() ;
    const random = new Date().getTime() ;

    const options: FileUploadOptions = {
      fileKey: 'photo' ,
      fileName: 'myImage_' + random + '.jpg' ,
      chunkedMode: false ,
      httpMethod: 'post' ,
      mimeType: 'image/jpeg',
        };
        console.log(options.fileName) ;
        this.url = options.fileName ;
    fileTransfer.upload(this.myphoto, 'http://fruitfu.com/upload/uploadFoto.php' , options)
    .then((data) => {
      alert('Success');
      loading.dismiss() ;
        }, (err) => {
          console.log(err);
          alert('Error');
          loading.dismiss();
        }) ;
  }
  photo() {
    const options: CameraOptions  = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY ,
      saveToPhotoAlbum: false

    } ;

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
       this.myphoto = 'data:image/jpeg;base64,' + imageData;
       console.log(this.myphoto) ;
     }, (err) => {
      // Handle error
      console.log(err) ;
     });
  }

  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/user/HomePage']) ;
  }

  async update( ) {
    this.url = '' ;
    console.log(this.myphoto);
    // tslint:disable-next-line:triple-equals
    if ( this.myphoto != undefined ) { this.click() ; }
    this.id = localStorage.getItem('token') ;
    console.log(this.id) ;
    const nom = (<HTMLInputElement>document.getElementById('name')).value ;
    const last = (<HTMLInputElement>document.getElementById('prenom')).value ;
    const pass = (<HTMLInputElement>document.getElementById('pass')).value ;
    const file = (<HTMLInputElement>document.getElementById('file')).value ;
    // tslint:disable-next-line:triple-equals
    if ( nom == '' && last == '' && pass == '' && file == '') {
      const toastt =  await this.cntr.create({
        message: 'أدرج التغييرات قبل ذلك  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();


    } else {
    // tslint:disable-next-line:radix
     await this.user.getSellerById(parseInt(this.id)).toPromise().then( data => { this.open = data ; } ) ;
     this.name = this.open.first_name ;
     this.prenom = this.open.last_name ;
     this.password = this.open.password ;
     // tslint:disable-next-line:triple-equals
     if ( nom != '') { this.name = nom  ; }
     // tslint:disable-next-line:triple-equals
     if ( last != '') { this.prenom = last  ; }
     // tslint:disable-next-line:triple-equals
     if ( pass != '') { this.password = pass  ; }
     // tslint:disable-next-line:triple-equals
     if (this.url == '') { this.url = this.open.url ; }
     // tslint:disable-next-line:radix
     await this.service.update_user(parseInt(this.id) , this.name , this.prenom , this.password , this.url ).toPromise()
     .then(data => {
       console.log(data) ;
     });
     this.app.fun();
    }
  }

}
