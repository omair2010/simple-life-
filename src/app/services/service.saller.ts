import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient , HttpHeaders   } from '@angular/common/http' ;
@Injectable({providedIn: 'root'})

export class SellerService {
  apiKey = '44958640bbe59102e04855463f0f5efe' ;
  url;

  constructor (private http: HttpClient ) {
    this.url = 'http://fruitfu.com/api/'   ;
  }


    getSeller() {
     const time = new Date() ;
     time.getTime() ;
     const md5 = require('md5') ;
     const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
     const headers = new HttpHeaders({
       'X-Authorization-Token':  token ,
       'X-Authorization-Time': time.getTime() + '',

       });

    return this.http.get(this.url + 'users');

      }
      getSellerById(id: number) {
        const time = new Date() ;
        time.getTime() ;
        const md5 = require('md5') ;
        const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
        const headers = new HttpHeaders({
          'X-Authorization-Token':  token ,
          'X-Authorization-Time': time.getTime() + '',
          });
       return this.http.get( this.url + 'getuser?id=' + id);
         }

         getSellerMail(email: string) {
          const time = new Date() ;
          time.getTime() ;
          const md5 = require('md5') ;
          const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
          const headers = new HttpHeaders({
            'X-Authorization-Token':  token ,
            'X-Authorization-Time': time.getTime() + '',
            });
         return this.http.get( this.url + 'login_mail?email=' + email );
           }
           ListeByID(owner_pub: number) {
            const time = new Date() ;
            time.getTime() ;
            const md5 = require('md5') ;
            const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
            const headers = new HttpHeaders({
              'X-Authorization-Token':  token ,
              'X-Authorization-Time': time.getTime() + ''
              });
           return this.http.get(this.url + 'pub_of_seller?owner_pub=' + owner_pub );
             }

             loginPassword(email: string , password: string) {
              const time = new Date() ;
              time.getTime() ;
              const md5 = require('md5') ;
              const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
              const headers = new HttpHeaders({
                'X-Authorization-Token':  token ,
                'X-Authorization-Time': time.getTime() + '',
                });
             return this.http.get(this.url + 'user_login_password?email=' + email + '&password=' + password);
               }

               addProject(nom: string , owner_pub: number , categorie_id: number , x: string , lng: string , description: string) {
                 console.log(lng);
               return this.http.post(this.url + 'addproject',
               { name_pub: nom ,
                owner_pub: owner_pub,
                categorie_id: categorie_id,
                x: x,
                y: lng,
                description: description,
                date: new Date()
               });
                 }

                 repass(email: string , password: string ) {

                 return this.http.post(this.url + 'repass' ,
                 {
                   email: email,
                   password: password
                 });
                   }


    verif() {
      const time = new Date() ;
     time.getTime() ;
     const md5 = require('md5') ;
     const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
     const headers = new HttpHeaders({
       'X-Authorization-Token':  token ,
       'X-Authorization-Time': time.getTime() + ''


       });
       return this.http.get(this.url + 'mail' ) ;
    }

      mail( mail: string , code: string  ) {
        const time = new Date() ;
        time.getTime() ;
        const md5 = require('md5') ;
        const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
        const headers = new HttpHeaders({
          'X-Authorization-Token':  token ,
          'X-Authorization-Time': time.getTime() + ''

          });
        return this.http.post(this.url + 'verification_code' ,
        { email: mail ,
          code: code  } ) ;
      }




    // tslint:disable-next-line:max-line-length
    addSeller( first_name: string , last_name: String , email: String , phone: string , country: string , password: string , type: string , date_signup: Date) {

      const time = new Date() ;
      time.getTime() ;
      const md5 = require('md5') ;
      const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
      const headers = new HttpHeaders({
        'X-Authorization-Token':  token ,
        'X-Authorization-Time': time.getTime() + '',

        });

      return this.http.post( this.url + 'user', {
        first_name: first_name,
        last_name: last_name,
        email: email,
        phone: phone,
        country: country ,
        password: password ,
        type: type ,
        date_signup: date_signup
        }) ;
    }

    addUser( first_name: string , last_name: String , email: String , password: string , type: string , date_signup: Date) {

      const time = new Date() ;
      time.getTime() ;
      const md5 = require('md5') ;
      const token = md5(this.apiKey + time.getTime() + navigator.userAgent ) ;
      const headers = new HttpHeaders({
        'X-Authorization-Token':  token ,
        'X-Authorization-Time': time.getTime() + '',

        });

      return this.http.post( this.url + 'add_user', {
        first_name: first_name,
        last_name: last_name,
        email: email,
        password: password ,
        type: type ,
        date_signup: date_signup
        }) ;
    }

    deletePub(id: number) {
       return this.http.get(this.url + 'delete_pub?id=' + id ) ;
    }

    editPub(id: number , name_pub: string , categorie_id: number , description: string) {
      return this.http.post(this.url + 'update_of_pub' , {
      id: id ,
      name_pub: name_pub ,
      categorie_id: categorie_id ,
      description: description ,
      date: new Date()
      }) ;
   }

    messages(subject: string, contenu: string , from_id: number , to_id: number , nom: string ) {
      return this.http.post(this.url + 'add_message' , {
        subject: subject ,
        contenu: contenu ,
        from_id: from_id ,
        to_id: to_id ,
        date: new Date() ,
        nom: nom
      }) ;
   }

   pubById(id: number) {
    return this.http.get(this.url + 'pub_of_id?id=' + id ) ;
 }

 get_demande() {
  return this.http.get(this.url + 'get_demande' ) ;
}

all_msg(id: number) {
  return this.http.get(this.url + 'all_msg_sended_to_user?to_id=' + id  ) ;

}

msgById(id: number) {
  return this.http.get(this.url + 'msg_by_id?id=' + id  ) ;

}
}
