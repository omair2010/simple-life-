import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({ providedIn: 'root' })

export class UserService {
  apiKey = '44958640bbe59102e04855463f0f5efe';
  url;

  constructor(private http: HttpClient) {
    this.url = 'http://fruitfu.com/api/';
  }

  addUser(first_name: string, last_name: String, email: String, password: string, type: string, date_signup: Date) {

    const time = new Date();
    time.getTime();
    const md5 = require('md5');
    const token = md5(this.apiKey + time.getTime() + navigator.userAgent);
    const headers = new HttpHeaders({
      'X-Authorization-Token': token,
      'X-Authorization-Time': time.getTime() + ''
    });

    return this.http.post(this.url + 'add_user', {
      first_name: first_name,
      last_name: last_name,
      email: email,
      password: password,
      type: type,
      date_signup: date_signup
    });
  }

  getPositionById(id: number) {
    const time = new Date();
    time.getTime();
    const md5 = require('md5');
    const token = md5(this.apiKey + time.getTime() + navigator.userAgent);
    const headers = new HttpHeaders({
      'X-Authorization-Token': token,
      'X-Authorization-Time': time.getTime() + ''
    });
    return this.http.get(this.url + 'getposition?id=' + id);
  }
  getPubs(id: number) {
    const time = new Date();
    time.getTime();
    const md5 = require('md5');
    const token = md5(this.apiKey + time.getTime() + navigator.userAgent);
    const headers = new HttpHeaders({
      'X-Authorization-Token': token,
      'X-Authorization-Time': time.getTime() + ''
    });
    return this.http.get(this.url + 'pubs_of_categorie?categorie_id=' + id);
  }

  getPubsById(id: number) {
    const time = new Date();
    time.getTime();
    const md5 = require('md5');
    const token = md5(this.apiKey + time.getTime() + navigator.userAgent);
    const headers = new HttpHeaders({
      'X-Authorization-Token': token,
      'X-Authorization-Time': time.getTime() + ''
    });
    return this.http.get(this.url + 'pub_of_id?id=' + id);
  }

  postComment(pub_id: number, contenu: string, user_id: number) {
    return this.http.post(this.url + 'add_comment', {
      pub_id: pub_id,
      contenu: contenu,
      user_id: user_id,
      date: new Date().toString()
    });
  }

  getCommentById(id: number) {
    return this.http.get(this.url + 'comments_of_pub?pub_id=' + id);
  }

  update_user(id: number, first_name: string, last_name: string, password: string, url: string) {
    return this.http.post(this.url + 'update_user', {
      id: id,
      first_name: first_name,
      last_name: last_name,
      password: password,
      url: url
    });
  }


  add_demande(id_user: number, id_seller: string, contenu: string, nom: string) {
    return this.http.post(this.url + 'add_demande', {
      id_user: id_user,
      id_seller: id_seller,
      contenu: contenu,
      nom: nom
    });
  }

  report(id_user: number, report: string) {
    return this.http.post(this.url + 'report', {
      id_user: id_user,
      report: report
    });
  }

  report_pub(id_user: number, report: string, id_pub: number) {
    return this.http.post(this.url + 'report_pub', {
      id_user: id_user,
      report: report,
      id_pub: id_pub
    });
  }

  blocage(id_pub: number, id_user: string, description: string) {
    return this.http.post(this.url + 'blocage', {
      id_pub: id_pub,
      id_user: id_user,
      description: description,
      statut: 0
    });
  }
  affiche(id_pub: number, id_user: string) {
    return this.http.get(this.url + 'liste_blocage?id_pub=' + id_pub + '&id_user=' + id_user);
  }



}
