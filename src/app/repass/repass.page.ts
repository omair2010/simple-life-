import { ActivatedRoute } from '@angular/router';
import { SellerService } from './../services/service.saller';
import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-repass',
  templateUrl: './repass.page.html',
  styleUrls: ['./repass.page.scss'],
})
export class RepassPage implements OnInit {
  params: any ;
  x: Object;
  var: any ;
  constructor(private ctrl: ToastController ,  private service: SellerService, private route: ActivatedRoute,
    private nav: NavController) { }

  ngOnInit() {
    this.route.params.pipe().subscribe(params => {
      this.params = params ;
     });
     console.log(this.params);
  }
  async click() {
    const pass = (<HTMLInputElement>document.getElementById('pass')).value ;
    const repass = (<HTMLInputElement>document.getElementById('repass')).value ;
    if ( pass.length < 6  ) {
      const toastt =  await this.ctrl.create({
        message: 'كلمة السر قصيرة  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    // tslint:disable-next-line:triple-equals
    } else if ( pass != repass ) {
      const toastt =  await this.ctrl.create({
        message: 'كلمة السر غير متطابقة   ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    // tslint:disable-next-line:triple-equals
    } else if ( pass == repass ) {
      await this.service.repass(this.params.mail , pass).toPromise().then(data => { console.log(data ) ;
      this.x = data; }) ;
      const toastt =  await this.ctrl.create({
        message: 'تم تغيير كلمة المرور بنجاح  ',
        duration: 1000,
        position: 'bottom'
      });
      toastt.present();
      await this.service.getSellerMail(this.params.mail).toPromise().then( data => { this.var = data ; } );
      console.log(this.var.type) ;
      // tslint:disable-next-line:triple-equals
      if ( this.var.type == 's' ) {
        this.nav.navigateRoot(['/loginSeller']) ;
      } else {
        this.nav.navigateRoot(['/loginUser']) ;
      }

    }
    }

  }

