import { Component, OnInit } from '@angular/core';
import { SellerService } from '../services/service.saller';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators , FormControl} from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-reg',
  templateUrl: './reg.page.html',
  styleUrls: ['./reg.page.scss'],
})
export class RegPage implements OnInit {
  mails: any;
  i = 0;
  index: number;
  constructor( private sing: SellerService, private nav: NavController , private ctrl: ToastController ,
    private loadingCtrl: LoadingController , private formBuilder: FormBuilder, private route: Router) {
    this.form = this.formBuilder.group({
      nom : [null , [Validators.compose ( [Validators.required , Validators.maxLength(25) ] )] ] ,
     prenom : [ null, [Validators.compose ([ Validators.required , Validators.maxLength(25) ] ) ] ] ,
      mail : [ null, [ Validators.compose (
        [Validators.required ,
           Validators.email ,
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$') ] ) ]  ],
      tel : [ null, Validators.required],
      country : [ null, Validators.required ],
      pass : [null , [Validators.compose ( [Validators.required , Validators.minLength(6) ] )] ] ,
      repass : [ null, Validators.required ]
     }) ;
}
  form: FormGroup ;
  type: any ;
  validation_messages = {
    'nom': [
        { type: 'required', message: 'يجب عليك وضع الإسم الأول.' },
        { type: 'maxlength', message: 'لقد تجاوزت 25 حرفا .' }

      ],
      'prenom': [
        { type: 'required', message: 'يجب عليك وضع الإسم الأخير.' },
        { type: 'maxlength', message: 'لقد تجاوزت 25 حرفا .' }

      ],
      'mail': [
        { type: 'required', message: 'يجب عليك وضع البريد الإلكتروني.' },
        { type: 'pattern', message: 'تأكد من البريد الإلكتروني.' }
      ],
      'tel': [
        { type: 'required', message: 'يجب عليك وضع رقم الجوال.' },
      ],
      'country': [
        { type: 'required', message: 'يجب عليك وضع الدولة/المدينة.' },
      ],
      'pass': [
        { type: 'required', message: 'يجب عليك وضع كلمة المرور .' },
        { type: 'minLength', message: 'كلمة المرور يجب أن تكون أكبر من 6 حروف .' }
      ],
      'repass': [
        { type: 'required', message: 'يجب عليك وضع تأكيد كلمة المرور  .' },
      ]
        } ;


  ngOnInit() {
  }



  async click() {

    const nom = (<HTMLInputElement>document.getElementById('nom')).value ;
    const prenom =  (<HTMLInputElement>document.getElementById('prenom')).value ;
    const mail = (<HTMLInputElement>document.getElementById('barid')).value ;
    const tel = (<HTMLInputElement>document.getElementById('hatef')).value ;
    const country = (<HTMLInputElement>document.getElementById('country')).value ;
    const mot = (<HTMLInputElement>document.getElementById('passs')).value ;
    const repass = (<HTMLInputElement>document.getElementById('repass')).value ;
    console.log(mot) ;
    console.log(repass) ;


    if ( nom == null || prenom == null || mail == null || tel == null || country == null || mot == null || repass == null) {
      const toastt =  await this.ctrl.create({
        message: ' لم تتم تعمير كل الخانات ',
        duration: 2000,
        position: 'middle'
      });
      toastt.present();
     // tslint:disable-next-line:triple-equals
    // tslint:disable-next-line:radix
    } else if ( parseInt( mot) != parseInt( repass ) ) {
       const toastt =  await this.ctrl.create({
        message: 'كلمة السر غير متطابقة  :-(',
        duration: 2000,
        position: 'middle'
      });
      toastt.present();
    } else {
      await this.sing.verif().toPromise().then(data => { this.mails = data ; console.log(this.mails.data) ; });
      this.i = 0 ;
      console.log(this.mails.data.length) ;
      this.index = 0 ;
      for ( const m of this.mails.data ) {
       // tslint:disable-next-line:triple-equals
       if ( m.email == mail  ) {
        this.index = this.i ;
       }
       this.i ++ ;
      }
      if ( this.i + 1 > this.mails.data.length ) {
        console.log('yes') ;
        const loading = await  this.loadingCtrl.create({
          message: 'يرجى الانتظار ...'
        });
        loading.present();
    await this.sing.addSeller( nom , prenom , mail , tel , country , mot , 's' , new Date() )
    .toPromise().then(async data => { this.type = data ; console.log( this.type.api_status ) ;
      this.nav.navigateRoot('/loginSeller');
      loading.dismiss() ;
      this.route.navigate(['/loginSeller']);
      const toastt =  await this.ctrl.create({
        message: 'تم التسجيل بنجاح',
        duration: 1000,
        position: 'middle'
      });
      toastt.present();
       })
    .catch( (err) => {
      loading.dismiss();
      alert(err) ;
    } );
      } else {
    const toastt =  await this.ctrl.create({
      message: 'البريد الإلكتروني مستعمل',
      duration: 1500,
      position: 'middle'
    });
    toastt.present();

  }
  }
    }
    goback() {
      console.log('hi' );
      this.nav.navigateRoot(['/selection']) ;
    }
}
