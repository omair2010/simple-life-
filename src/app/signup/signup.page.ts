import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { SellerService } from '../services/service.saller';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../services/service.user';
import { delay } from 'q';
import { HttpClient } from '@angular/common/http' ;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  mails: any;
  index: number;
  i: number;
  type: any;
  form: FormGroup ;
  validation_messages = {
    'nom': [
        { type: 'required', message: 'يجب عليك وضع الإسم الأول.' },
        { type: 'maxlength', message: 'لقد تجاوزت 25 حرفا .' }

      ],
      'prenom': [
        { type: 'required', message: 'يجب عليك وضع الإسم الأخير.' },
        { type: 'maxlength', message: 'لقد تجاوزت 25 حرفا .' }

      ],
      'mail': [
        { type: 'required', message: 'يجب عليك وضع البريد الإلكتروني.' },
        { type: 'pattern', message: 'تأكد من البريد الإلكتروني.' }
      ],
      'tel': [
        { type: 'required', message: 'يجب عليك وضع رقم الجوال.' },
      ],
      'pass': [
        { type: 'required', message: 'يجب عليك وضع كلمة المرور .' },
        { type: 'minLength', message: 'كلمة المرور يجب أن تكون أكبر من 6 حروف .' }
      ]
        } ;

  constructor(private service: UserService , private sing: SellerService , private nav: NavController , private http: HttpClient,
    private formBuilder: FormBuilder , private ctrl: ToastController , private loadingCtrl: LoadingController) {
        this.form = this.formBuilder.group({
          nom : [null , [Validators.compose ( [Validators.required , Validators.maxLength(25) ] )] ] ,
         prenom : [ null, [Validators.compose ([ Validators.required , Validators.maxLength(25) ] ) ] ] ,
          mail : [ null, [ Validators.compose (
            [Validators.required ,
               Validators.email ,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$') ] ) ]  ],
          pass : [null , [Validators.compose ( [Validators.required , Validators.minLength(6) ] )] ] ,
         }) ;
     }

  ngOnInit() {
  }

  async click() {

    const nom = (<HTMLInputElement>document.getElementById('nom')).value ;
    const prenom =  (<HTMLInputElement>document.getElementById('prenom')).value ;
    const pass =  (<HTMLInputElement>document.getElementById('facebook')).value ;
    const mail =  (<HTMLInputElement>document.getElementById('yahoo')).value ;
await delay(300) ;
console.log( nom );
console.log( prenom );

    // tslint:disable-next-line:triple-equals
    if ( nom == '' || prenom == '' || mail == '' || pass == '' )  {
      const toastt =  await this.ctrl.create({
        message: 'لم تتم تعمير كل الخانات ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    // tslint:disable-next-line:triple-equals
    } else {
      await this.sing.verif().toPromise().then(data => { this.mails = data ; console.log(this.mails.data) ; });
      this.i = 0 ;
      console.log(this.mails.data.length) ;
      this.index = 0 ;
      for ( const m of this.mails.data ) {
       // tslint:disable-next-line:triple-equals
       if ( m.email == mail  ) {
        this.index = this.i ;
       }
       this.i ++ ;
      }
      console.log(this.mails.data[this.index].email) ;
      console.log(mail) ;

      // tslint:disable-next-line:whitespace
      if ( this.mails.data[this.index].email !== mail ) {
        const loading = await  this.loadingCtrl.create({
          message: 'يرجى الانتظار ...'
        });
        loading.present();

      await this.http
      // tslint:disable-next-line:max-line-length
      .get('http://fruitfu.com/api/add_user2?first_name=' + nom + '&last_name=' + prenom + '&email=' + mail + '&password=' + pass + '&type=u&date_signup=' + new Date())
      .toPromise()
      .then(async (data: any) => {
        this.type = data ;
        console.log( this.type ) ;
        this.nav.navigateRoot('/loginUser');
        const toast =  await this.ctrl.create({
          message: 'تم التسجيل بنجاح',
          duration: 1000,
          position: 'middle'
        });
        toast.present();
        loading.dismiss();
      })
      .catch( ( err) => {
        loading.dismiss();
        alert(err) ;
      } );

    /*  await this.service.addUser( nom , prenom , mail , pass , 'u' , new Date() )
    .toPromise().then(async (data: any) => {
      this.type = data ; console.log( this.type ) ;
      this.nav.navigateRoot('/loginUser');
      const toast =  await this.ctrl.create({
        message: 'تم التسجيل بنجاح',
        duration: 1000,
        position: 'middle'
      });
      toast.present();
      loading.dismiss();
    })
    .catch( ( err) => {
      loading.dismiss();
      alert(err) ;
    } );*/
      } else {
    const toastt =  await this.ctrl.create({
      message: 'البريد الإلكتروني مستعمل',
      duration: 1500,
      position: 'middle'
    });
    toastt.present();

  }
  }
    }
    goback() {
      console.log('hi' );
      this.nav.navigateRoot(['/loginUser']) ;
    }
}
