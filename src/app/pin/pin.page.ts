import { NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SellerService } from '../services/service.saller';

@Component({
  selector: 'app-pin',
  templateUrl: './pin.page.html',
  styleUrls: ['./pin.page.scss'],
})
export class PinPage implements OnInit {
code: any ;
  constructor(private route: ActivatedRoute , private nav: NavController , private ctrl: ToastController ,
     private service: SellerService ) { }

  ngOnInit() {
    this.route.params.pipe().subscribe(params => {
      this.code = params ;
     });
     console.log(this.code);
  }

  async click() {
    const test = (<HTMLInputElement>document.getElementById('test')).value ;
    // tslint:disable-next-line:triple-equals
    if ( this.code.code == test ) {
        this.nav.navigateRoot(['/repass/' + this.code.mail]) ;
    } else { const toastt =  await this.ctrl.create({
      message: 'تأكد من رامز التفعيل ',
      duration: 2000,
      position: 'bottom'
    });
    toastt.present(); }
  }
  async again() {
       this.service.mail(this.code.mail ,  this.code.code.toString() ).toPromise().then() ;
      const toastt =  await this.ctrl.create({
        message: 'تم إعادة إرسال رمز التفعيل ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    }
    goback() {
      console.log('hi' );
      this.nav.navigateRoot(['/password']) ;
    }
  }

