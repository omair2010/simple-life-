import { Component, OnInit } from '@angular/core';
import { Slides, NavController } from '@ionic/angular';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.page.html',
  styleUrls: ['./step1.page.scss'],
})

export class Step1Page implements OnInit {
  open = false ;

  constructor( private nav: NavController) { }

  ngOnInit() {
  }
  ionSlideDidChange() {
    console.log('hi') ;
  }
  isEnd () {
     console.log('hi') ;
  }
  add() {
   this.nav.navigateRoot(['/selection']) ;
  }
  ionSlideReachEnd() {
    console.log('hi') ;
    this.open = true ;
  }
}
