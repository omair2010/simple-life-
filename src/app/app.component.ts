import { Component } from '@angular/core';

import { Platform, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { SellerService } from './services/service.saller';
import { UserService } from './services/service.user';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  id = '';
  open: any;
  x = false;
  name: string;
  url: string;
  constructor(
    private rapport: UserService ,
    private alertController: AlertController ,
    private user: SellerService ,
    private nav: NavController ,
    private service: AuthService ,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    console.log(this.id) ;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  click() {
    const menu = document.querySelector('ion-menu-controller');
    menu.close() ;
    this.service.logout();
  }
  add() {
    const menu = document.querySelector('ion-menu-controller');
    menu.close() ;
    this.nav.navigateRoot('seller/home') ;
  }
  liste() {
    const menu = document.querySelector('ion-menu-controller');
    menu.close() ;
    this.nav.navigateRoot('seller/listes') ;
  }

  async fun() {
    this.url = null ;
    this.id = localStorage.getItem('token') ;
    console.log(this.id) ;
    // tslint:disable-next-line:radix
     await this.user.getSellerById(parseInt(this.id)).toPromise().then( data => { this.open = data ; } ) ;
     console.log(this.open) ;
     // tslint:disable-next-line:triple-equals
     if (this.open.url != null ) {
       this.url = 'http://fruitfu.com/upload/images/' + this.open.url ;
       console.log(this.url);
     }
    // tslint:disable-next-line:triple-equals
    this.x = this.open.type == 's' ;
    this.name = this.open.first_name ;
    console.log(this.name) ;
  }

  setting() {
    const menu = document.querySelector('ion-menu-controller');
    menu.close() ;
    // tslint:disable-next-line:triple-equals
    if ( this.open.type == 'u' ) {
      this.nav.navigateRoot('/user/setting') ;

    } else {
      this.nav.navigateRoot('/seller/settings') ;

    }
    }
    demande() {
      const menu = document.querySelector('ion-menu-controller');
      menu.close() ;
      this.nav.navigateRoot('seller/demande') ;
  }
  msg() {
    const menu = document.querySelector('ion-menu-controller');
      menu.close() ;
      this.nav.navigateRoot('seller/messages') ;
  }

  async report() {
    const alert = await this.alertController.create({
      header: ' إتصل بنا    ',
      inputs: [
        {
          name: 'name1',
          type: 'text',
          id: 'text' ,
          placeholder: 'إتصل بنا  '
        }
      ],
      buttons: [
        {
          text: 'إلغاء ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'إرسال',
          handler: () => {
            this.fn();
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
  }
  fn() {
    const text = (<HTMLInputElement>document.getElementById('text')).value ;
    // tslint:disable-next-line:radix
    this.rapport.report(parseInt( this.id ) , text).subscribe() ;
  }
}
