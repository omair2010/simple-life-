import { Component, OnInit } from '@angular/core';
import {
  GoogleMaps, GoogleMap
} from '@ionic-native/google-maps/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-marker',
  templateUrl: './marker.page.html',
  styleUrls: ['./marker.page.scss'],
})
export class MarkerPage implements OnInit {
  map: GoogleMap;

  constructor(private platform: Platform ,  private googleMaps: GoogleMaps) { }

  async ngOnInit() {
    // Since ngOnInit() is executed before `deviceready` event,
    // you have to wait the event.
    await this.platform.ready();
    await this.loadMap();
  }

  loadMap() {

    this.map = this.googleMaps.create('map_canvas', {
      'camera': {
        'target': {
          'lat': 21.382314,
          'lng': -157.933097
        },
        'zoom': 10
      }
    });
    }

}
