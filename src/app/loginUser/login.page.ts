import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { SellerService } from '../services/service.saller';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  users: any;
  i = 0;
  index: number;
  verif: any;
  constructor(private nav: NavController , private loadingCtrl: LoadingController ,
     private service: SellerService , private ctrl: ToastController ) {
  }

  async ngOnInit() {
  }
  async click() {
    const mail = (<HTMLInputElement>document.getElementById('mail')).value ;
    const pass =  (<HTMLInputElement>document.getElementById('pass')).value ;
    console.log(mail);
    console.log(pass) ;
    // tslint:disable-next-line:triple-equals
    if ( mail == '' && pass == ''  ) {
      const toastt =  await this.ctrl.create({
        message: 'لم تضع البريد ورقم السري ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    // tslint:disable-next-line:triple-equals
    } else if ( mail == ''  ) {
      const toastt =  await this.ctrl.create({
        message: 'لم تضع البريد  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    // tslint:disable-next-line:triple-equals
    } else if ( pass == '' ) {
      const toastt =  await this.ctrl.create({
        message: 'لم تضع رقم السري  ',
        duration: 2000,
        position: 'bottom'
      });
      toastt.present();
    } else { this.login(mail , pass ) ; }
  }

  async login( mail , pass ) {
    const loading = await  this.loadingCtrl.create({
      message: 'يرجى الانتظار ...'
    });
    loading.present();
    this.verif = undefined ;
  await this.service.getSellerMail(mail).toPromise()
  .then( data => { this.users = data ; } )
  .catch( () => {
    loading.dismiss();
      alert('تثبت من اتصالك بشبكة الأنترنت ') ;
  } ) ;
 await this.service.loginPassword(mail , pass ).toPromise()
 .then(data => { console.log(data) ; this.verif = data ; })
 .catch( () => {
  loading.dismiss();
    alert('تثبت من اتصالك بشبكة الأنترنت ') ;
} ) ;
  // tslint:disable-next-line:triple-equals
  if (this.users.email == mail && this.users.type  == 'u' ) {
     // tslint:disable-next-line:triple-equals
     if ( this.verif.id != undefined  ) {
       console.log('login') ;
       localStorage.setItem('isLoggedIn', 'true');
       localStorage.setItem('token', this.users.id);
       this.nav.navigateRoot(['/user/HomePage']) ;
       loading.dismiss();
     } else { console.log( 'password invalid' ) ;
     loading.dismiss();
     const toastt =  await this.ctrl.create({
      message: 'الرقم السري خاطئ  ',
      duration: 2000,
      position: 'bottom'
    });
    toastt.present();
   }
    } else { console.log( 'mail not found' );
    loading.dismiss();
    const toastt =  await this.ctrl.create({
      message: 'البريد الالكتروني خاطئ',
      duration: 2000,
      position: 'bottom'
    });
    toastt.present(); }
  }

  goback() {
    console.log('hi' );
    this.nav.navigateRoot(['/selection']) ;
  }

  href() {
    this.nav.navigateRoot(['/password']) ;
  }

}
